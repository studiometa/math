/* ================================
 * Math utilities
 * ================================ */



/**
 * Get the distance between 2 points in pixels
 *
 * @param  {Object} a The first point object
 * @param  {Object} b The second point object
 * @return {Number}   The distance in pixel between the 2 points
 */
export function getDistanceBetween(a, b) {
	const deltaX = Math.abs(a.x - b.x)
	const deltaY = Math.abs(a.y - b.y)
	return Math.sqrt(deltaX * deltaX + deltaY * deltaY)
}



/**
 * Get the angle between 2 points in radians
 *
 * @param  {Object} a The first point object
 * @param  {Object} b The second point object
 * @return {Number}   The angle between the 2 points in radians
 */
export function getAngleBetween(a, b) {
	return Math.atan2(b.x - a.x, b.y - a.y)
}



/**
 * Convert a radian angle to degrees
 *
 * @param  {Number} angle The angle to convert
 * @return {Number}       The converted angle in degrees
 */
export function radToDeg(angle) {
	return angle * 180 / Math.PI
}



/**
 * Convert a degree angle in radian
 *
 * @param  {Number} degrees The angle to convert
 * @return {Number}         The converted angle in radians
 */
export function degToRad(degrees) {
	return degrees * Math.PI / 180
}



/**
 * Get a random integer in the given interval
 * @param  {integer} min The interval's minimum
 * @param  {integer} max The interval's maximum
 * @return {integer}     A random integer in the given interval
 */
export function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min)) + min
}
